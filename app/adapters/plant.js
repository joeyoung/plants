import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
	buildURL: function () {
		return 'http://localhost:9080/sample/api/plants';
	}
});
