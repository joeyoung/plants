import Ember from 'ember';

export default Ember.Route.extend({
	model: function () {
		return this.store.createRecord('plant');
	},
	actions: {
		save: function () {
			console.log('save');
			var self = this;
			var model = this.modelFor('plants/new');
			model.save().then(function () {
				self.transitionTo('plants');
			});
		}
	},
	deactivate: function () {
		var model = this.modelFor('plants/new');
		if (model.get('isNew')) {
			model.destroyRecord();
		}
	}
});
